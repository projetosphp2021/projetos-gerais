<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['id' => '1', 'parent_id' => NULL, 'name' => 'Peças Reposição E Manutenção', 'photo_url' => NULL],
            ['id' => '2', 'parent_id' => NULL, 'name' => 'Ordem de Serviço', 'photo_url' => NULL],
            ['id' => '3', 'parent_id' => NULL, 'name' => 'Acessórios', 'photo_url' => NULL],
            ['id' => '4', 'parent_id' => NULL, 'name' => 'Ar Condicionado', 'photo_url' => NULL],
            ['id' => '5', 'parent_id' => NULL, 'name' => 'Baterias', 'photo_url' => NULL],
            ['id' => '6', 'parent_id' => NULL, 'name' => 'Brinquedos', 'photo_url' => NULL],
            ['id' => '7', 'parent_id' => NULL, 'name' => 'Cabos', 'photo_url' => NULL],
            ['id' => '8', 'parent_id' => NULL, 'name' => 'Caixas De Som', 'photo_url' => NULL],
            ['id' => '9', 'parent_id' => NULL, 'name' => 'Capinhas', 'photo_url' => NULL],
            ['id' => '10', 'parent_id' => NULL, 'name' => 'Carregadores', 'photo_url' => NULL],
            ['id' => '11', 'parent_id' => NULL, 'name' => 'Cartão Memória', 'photo_url' => NULL],
            ['id' => '12', 'parent_id' => NULL, 'name' => 'Celular', 'photo_url' => NULL],
            ['id' => '13', 'parent_id' => NULL, 'name' => 'Controloe Remoto', 'photo_url' => NULL],
            ['id' => '14', 'parent_id' => NULL, 'name' => 'Fone de Ouvido', 'photo_url' => NULL],
            ['id' => '15', 'parent_id' => NULL, 'name' => 'Fontes/Cabos', 'photo_url' => NULL],
            ['id' => '16', 'parent_id' => NULL, 'name' => 'Geral', 'photo_url' => NULL],
            ['id' => '17', 'parent_id' => NULL, 'name' => 'Informática', 'photo_url' => NULL],
            ['id' => '18', 'parent_id' => NULL, 'name' => 'Modens', 'photo_url' => NULL],
            ['id' => '19', 'parent_id' => NULL, 'name' => 'Películas', 'photo_url' => NULL],
            ['id' => '20', 'parent_id' => NULL, 'name' => 'Relógios', 'photo_url' => NULL],
            ['id' => '21', 'parent_id' => NULL, 'name' => 'Roteadores', 'photo_url' => NULL],
            ['id' => '22', 'parent_id' => NULL, 'name' => 'Som', 'photo_url' => NULL],
            ['id' => '23', 'parent_id' => NULL, 'name' => 'Tablets', 'photo_url' => NULL],
            ['id' => '24', 'parent_id' => NULL, 'name' => 'TV', 'photo_url' => NULL],
            ['id' => '25', 'parent_id' => NULL, 'name' => 'Vídeo Games', 'photo_url' => NULL],

            ['id' => '26', 'parent_id' => 17, 'name' => 'Notebook', 'photo_url' => NULL],
            ['id' => '27', 'parent_id' => 17, 'name' => 'Teclados', 'photo_url' => NULL],
            ['id' => '28', 'parent_id' => 17, 'name' => 'Mouse', 'photo_url' => NULL],
            ['id' => '29', 'parent_id' => 17, 'name' => 'Pendrive', 'photo_url' => NULL],

            ['id' => '30', 'parent_id' => 25, 'name' => 'Console', 'photo_url' => NULL],
            ['id' => '31', 'parent_id' => 25, 'name' => 'Jogos', 'photo_url' => NULL],

            ['id' => '32', 'parent_id' => 17, 'name' => 'Novos', 'photo_url' => NULL],
            ['id' => '33', 'parent_id' => 17, 'name' => 'Usados', 'photo_url' => NULL],
        ]);
    }
}
