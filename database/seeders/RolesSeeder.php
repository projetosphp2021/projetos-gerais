<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create(['name' => 'caixa']);
        $role = Role::create(['name' => 'vendedor']);
        $role = Role::create(['name' => 'técnico']);
        $role = Role::create(['name' => 'admin']);
    }
}
