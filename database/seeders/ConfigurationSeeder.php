<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configurations')->insert([
            [
                'key' => 'company_name',
                'value' => 'Shop Cell'
            ],
            [
                'key' => 'email',
                'value' => 'contato@lojashoppcell.com.br'
            ],
            [
                'key' => 'logo',
                'value' => '',
            ],
            [
                'key' => 'cnpj',
                'value' => '12345679898'
            ],
            [
                'key' => 'cellphone',
                'value' => '(37) 3226-1750 ou Whatsapp (37) 99123-4321'
            ],
            [
                'key' => 'address',
                'value' => 'R. Cel. Martinho Ferreira do Amaral, 811-901 - Centro, Nova Serrana - MG, 35519-000'
            ],
            [
                'key' => 'bussiness_hours',
                'value' => 'Seg a Sex das 8am as 19h e Sáb. das 8am as 13h',
            ],
            [
                'key' => 'cupom_text',
                'value' => ''
            ],
            [
                'key' => 'warranty_text',
                'value' => ''
            ],
        ]);
    }
}
