<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TaxInstallments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_installments', function (Blueprint $table) {
            $table->increments('id')->unsigned()->nullable(); 
            $table->dateTime('date_start',50);
            $table->dateTime('date_finish',45);
            $table->decimal('interest rate',10,2)->nullable();
            $table->integer('max_number_stallment')->nullable();
                     
             
            $table->primary('id'); /*criação da chave primaria*/
            $table->timestamps(); /* data de criação do banco */
            $table->engine = 'InnoDB';

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tax_installments');
    }
}
