<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CashierOut extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashier_out', function (Blueprint $table) {
            $table->increments('id')->unsigned(); 
            $table->string('justification');
            $table->integer('payment_method_id')->nullable();
            $table->integer('bank_accounts_id')->nullable();
           
           
            $table->primary('id'); /*criação da chave primaria*/
            $table->timestamps(); /* data de criação do banco */
            $table->engine = 'InnoDB';

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cashier_out');
    }
}
