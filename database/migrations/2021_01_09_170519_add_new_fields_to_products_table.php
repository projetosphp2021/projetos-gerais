<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('is_excl_serv')->after('cost');
            $table->integer('commission_percentage')
                ->unsigned()
                ->nullable()
                ->after('can_commission');
            $table->integer('discount_percentage')
                ->unsigned()
                ->nullable()
                ->after('can_discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['is_excl_serv', 'commission_percentage', 'discount_percentage']);
        });
    }
}
