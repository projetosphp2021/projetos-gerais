<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code', 50);
            $table->string('barcode', 50)->nullable();
            $table->string('name', 50);
            $table->string('photo_url', 200)->nullable();
            $table->string('description', 120)->nullable();
            $table->decimal('price', 10, 2);
            $table->decimal('cost', 10, 2);
            $table->string('observation', 200)->nullable();
            $table->integer('minimum_stock');
            $table->boolean('can_discount');
            $table->boolean('can_commission');
            $table->boolean('is_replacement');
            $table->boolean('is_new');
            $table->boolean('is_active');
            $table->enum('type', ['un', 'kg', 'outro']);

            $table->foreignId('user_add_id')->constrained('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::table('products', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
