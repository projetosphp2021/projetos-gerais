<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BankAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() /*esse  método up é usado para adicionar novas tabelas, colunas ou índices ao seu banco de dados*/
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->increments('id')->unsigned();  
            $table->string('bank_name',50)->nullable();
            $table->string('agency',45)->nullable();
            $table->string('account_number',45);
            $table->tinyInteger('is_active',1);
           
            /*relaciomento  de tabelas */
            $table->primary('id'); /*criação da chave primaria*/
            $table->string('bank_name_id')->unsigned();
            $table->foreign('bank_name_id')->references('id')->on('bank_name');
            $table->timestamps(); /* data de criação do banco */
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bank_accounts');
    }
}
