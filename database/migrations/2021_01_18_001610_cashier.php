<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cashier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cashier', function (Blueprint $table) {
            $table->increments('id')->unsigned(); /* campo da chave primaria */
            $table->string('bank_name',50)->nullable();
            $table->string('agency',45)->nullable();
            $table->string('account_number',45);
            $table->boolean('is_active',1)->default(true);
            $table->timestamps(); /* data de criação do banco */


             /* campo da chave primaria */
             $table->primary('id'); /*criação da chave primaria*/
             $table->foreign('user_id')->references('id')->on('user_id');   /*Relaciomento das tabelas*/
             $table->timestamps(); /* data de criação do banco */
             $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cashier');
    }
}
