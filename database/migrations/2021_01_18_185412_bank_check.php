<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BankCheck extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() /*O método up() serve para definir a estrutura da tabela. */
    {
        Schema::create('BankCheck', function (Blueprint $table) {
            $table->increments('id')->unsigned(); /* Id da tabela (chave primária e incremento)*/
            $table->string('name',45)->nullable();/* Nome da categoria */
            $table->string('type_expense',45); /* Nome da categoria */
            $table->string('is_deposited',45); /* Nome da categoria */
            $table->decimal('value',10,2);
            $table->integer('sale_id',1);
            $table->integer('bank_account_id',1)->unsigned();
            $table->date('date_deposit',1);
           
           
            /*Relaciomento das tabelas */
            $table->primary('id'); /*criação da chave primaria*/
            $table->primary('sale_id'); /*criação da chave primaria*/
            $table->primary('bank_accounts'); /*criação da chave primaria*/
            $table->foreign('sale_id')->references('id')->on('sale_id')->onUpdate('cascade');;
            $table->foreign('bank_accounts')->references('id')->on('bank_accounts')->onDelete('cascade');
            $table->timestamps(); /* data de criação do banco */

            $table->engine = 'InnoDB';


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('BankCheck');
    }
}
