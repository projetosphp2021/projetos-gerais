<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');  
            $table->string('sale_type',50)->nullable();
            $table->decimal('total',10,2)->nullable();
            $table->bigInteger('salesman_id',20)->unsigned();
            $table->integer('payment_method_id',1)->default(true);
           
           
            /*Relaciomento das tabelas*/
           
            $table->primary('id'); /*criação da chave primaria*/
            $table->foreign('salesman_id')->references('id')->on('user_id');
            $table->timestamps(); /* data de criação do banco */
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales');
    }
}
