<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AlterProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('code');
            $table->decimal('cost', 10, 2)->nullable()->change();
            $table->integer('minimum_stock')->nullable()->change();
            DB::statement("ALTER TABLE `products` CHANGE `type` `type` ENUM('un', 'kg', 'outro') default NULL;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('code', 50);
            $table->decimal('cost', 10, 2)->change();
            $table->integer('minimum_stock')->change();
            DB::statement("ALTER TABLE `products` CHANGE `type` `type` ENUM('un', 'kg', 'outro') NOT NULL");
        });
    }
}
