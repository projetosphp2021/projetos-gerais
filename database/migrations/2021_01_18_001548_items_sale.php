<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ItemsSale extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_sale', function (Blueprint $table) {
            $table->increments('id')->unsigned(); /* campo da chave primaria */
            $table->string('status',1)->default(true);
            $table->string('sale_id',45)->nullable();
            $table->string('product_id',20);
            $table->decimal('value',10,2)->default(true);
            $table->integer('discount',10,2)->nullable(); 
            $table->bigInteger('user_approver_discount_id',);
            $table->string('discount_justification',45)->nullable();;
           
          
            $table->primary('id'); /*criação da chave primaria*/
            $table->foreignId('id')->references('id')->on('id');
            $table->timestamps(); /* data de criação do banco */
            $table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items_sale');
    }
}
