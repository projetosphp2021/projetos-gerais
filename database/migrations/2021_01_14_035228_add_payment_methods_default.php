<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentMethodsDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $methods = [
            'Cartão crédito',
            'Cartão débito',
            'Cartão Dinheiro',
            'Cartão Pix',
            'Cartão Cheque',
        ];

        foreach ($methods as $m) {
            \App\Models\PaymentMehods::create(['name' => $m]);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
