<?php

namespace App\Services\Address;

class UpdateAddressService
{

    public function run($owner, Array $data) {

        $data['postcode'] = preg_replace( '/[^0-9]/', '', $data['postcode'] );

        $address = [
            'postcode' => $data['postcode'],
            'street' => $data['street'],
            'neighborhood' => $data['neighborhood'],
            'number' => $data['number'],
            'city' => $data['city'],
            'state' => $data['state'],
        ];

        $owner->address()->update($address);
    }
}
