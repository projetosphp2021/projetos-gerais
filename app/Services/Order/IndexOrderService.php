<?php

namespace App\Services\Order;

use App\Models\Order;

class IndexOrderService
{
    /**
     * @var Order
     */
    private $order;

    /**
     * IndexOrderService constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function run($request)
    {
        $search = isset($request['search']) ? $request['search'] : '';

        return $this->order->when($search, function ($query, $search) {
            return $query->where('id', '=', $search);
        })->paginate(10);
    }
}
