<?php

namespace App\Services\Order;

use App\Models\Order;

class StoreOrderService
{
    /**
     * @var Order
     */
    private $order;

    /**
     * StoreOrderService constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param $data
     * @return array[]
     */
    public function run($data)
    {
        $order = $this->order->create($data);

        $orderProducts = [];

        foreach ($data['products'] as $key => $product) {
            $orderProducts[$product] = [
                'price' => $data['price'][$key],
                'discount' => $data['discount'][$key],
                'amount' => $data['amount'][$key]
            ];
        }

        $order->products()->sync($orderProducts);

        return $order;
    }
}
