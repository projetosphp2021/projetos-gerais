<?php

namespace App\Services\Order;

class UpdateOrderService
{
    /**
     * @param $data
     * @param $order
     * @return array[]
     */
    public function run($data, $order)
    {
        $order->update($data);

        $orderProducts = [];

        foreach ($data['products'] as $key => $product) {
            $orderProducts[$product] = [
                'price' => $data['price'][$key],
                'discount' => $data['discount'][$key],
                'amount' => $data['amount'][$key]
            ];
        }

        $order->products()->sync($orderProducts);

        return $order;
    }
}
