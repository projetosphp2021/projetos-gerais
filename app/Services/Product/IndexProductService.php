<?php

namespace App\Services\Product;

use App\Models\Product;

class IndexProductService
{
    /**
     * @var Product
     */
    private $product;

    /**
     * IndexProductService constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function run($request)
    {
        $search = isset($request['search']) ? $request['search'] : '';
        $query = $this->product->when($search, function ($query, $search) {
                return $query->where('name', 'like', '%' . $search . '%')->orWhere('barcode', 'like', '%' . $search . '%');
            });

        if ($request->paginate === 'false') {
            return $query->get();
        }

        return $query->paginate(10);
    }
}
