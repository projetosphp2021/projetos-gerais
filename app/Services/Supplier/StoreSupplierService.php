<?php

namespace App\Services\Supplier;

use App\Models\Supplier;
use App\Traits\UploadTrait;
use App\Services\Utilities\OnlyNumbersService;
use App\Services\Address\StoreAddressService;

class StoreSupplierService
{

    use UploadTrait;

    private $supplier;
    private $onlyNumbers;
    private $storeAddressService;

    public function __construct(
        Supplier $supplier,
        OnlyNumbersService $onlyNumbers,
        StoreAddressService $storeAddressService
    )
    {
        $this->supplier = $supplier;
        $this->onlyNumbers = $onlyNumbers;
        $this->storeAddressService = $storeAddressService;
    }

    public function run($data)
    {
        $data['photo_url'] = $this->uploadFile($data['photo'], 'public/suppliers_photos');
        $data['cnpj'] = $this->onlyNumbers->run($data['cnpj']);
        $data['state_registration'] = $this->onlyNumbers->run($data['state_registration']);
        $data['cellphone'] = $this->onlyNumbers->run($data['cellphone']);
        $data['phone'] = $this->onlyNumbers->run($data['phone']);

        $this->supplier = $this->supplier->create($data);
        $this->storeAddressService->run($this->supplier, $data);

        return $this->supplier;
    }
}
