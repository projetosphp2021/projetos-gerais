<?php

namespace App\Services\Supplier;

use App\Models\Supplier;
use App\Traits\UploadTrait;
use App\Services\Utilities\OnlyNumbersService;
use App\Services\Address\UpdateAddressService;

class UpdateSupplierService
{

    use UploadTrait;

    private $onlyNumbers;
    private $updateAddressService;

    public function __construct(
        OnlyNumbersService $onlyNumbers,
        UpdateAddressService $updateAddressService
    )
    {
        $this->onlyNumbers = $onlyNumbers;
        $this->updateAddressService = $updateAddressService;
    }

    public function run(Supplier $supplier, Array $data)
    {
        $data['photo_url'] = $this->uploadFile($data['photo'], 'public/suppliers_photos');
        $data['cnpj'] = $this->onlyNumbers->run($data['cnpj']);
        $data['state_registration'] = $this->onlyNumbers->run($data['state_registration']);
        $data['cellphone'] = $this->onlyNumbers->run($data['cellphone']);
        $data['phone'] = $this->onlyNumbers->run($data['phone']);

        $supplier->update($data);
        $this->updateAddressService->run($supplier, $data);

        return $supplier;
    }
}
