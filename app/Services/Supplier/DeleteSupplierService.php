<?php

namespace App\Services\Supplier;

use App\Models\Supplier;

class DeleteSupplierService
{
    public function run(Supplier $supplier)
    {
        $supplier->address()->delete();
        return $supplier->delete();
    }
}
