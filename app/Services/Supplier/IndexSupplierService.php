<?php

namespace App\Services\Supplier;

use App\Models\Supplier;
use App\Traits\UploadTrait;
use App\Services\Utilities\OnlyNumbersService;

class IndexSupplierService
{

    use UploadTrait;

    private $supplier;
    private $onlyNumbers;
    private $storeAddressService;

    public function __construct(Supplier $supplier)
    {
        $this->supplier = $supplier;
    }

    public function run()
    {
        return $this->supplier->all();
    }
}
