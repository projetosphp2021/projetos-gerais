<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\Product\IndexProductService;
use App\Models\Category;

class ProductController extends Controller
{
    /**
     * @var IndexProductService
     */
    private $indexProductService;

    /**
     * ProductController constructor.
     * @param IndexProductService $indexProductService
     */
    public function __construct(IndexProductService $indexProductService)
    {
        $this->indexProductService = $indexProductService;
    }

    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $search = null;
        if ($request->search) {
            $search = $request->search;
        }
        $products = $this->indexProductService->run($request);

        if ($request->expectsJson()) {
            return response($products);
        }
        return view('products.index')
            ->with('products', $products)
            ->with('search', $search);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::with('children')
                        ->where('parent_id', null)
                        ->get();

        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $user = auth()->user();

        if($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName   = time() . '.' . $photo->getClientOriginalExtension();
            $data['photo_url'] = $photo->storeAs('public/products_photos', $fileName, 'local');
            $data['photo_url'] = str_replace('public/', '', $data['photo_url']);
        }

        $data['can_discount'] = isset($data['can_discount']) ? true : false;
        $data['can_commission'] = isset($data['can_commission']) ? true : false;
        $data['is_replacement'] = isset($data['is_replacement']) ? true : false;
        $data['is_new'] = isset($data['is_new']) ? true : false;
        $data['is_active'] = isset($data['is_active']) ? true : false;
        $data['is_excl_serv'] = isset($data['is_excl_serv']) ? true : false;

        $data['discount_percentage'] =
        $data['can_discount'] ?
        $data['discount_percentage'] : null;

        $data['commission_percentage'] =
        $data['can_commission'] ?
        $data['commission_percentage'] : null;

        $newProduct = new Product($data);

        $newProduct->user_add_id = $user->id;
        $newProduct->save();

        foreach($data['categories'] as $category) {
            $newProduct->categories()->attach($category);
        }


        return redirect()->route('products.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = Category::with('children')
                        ->where('parent_id', null)
                        ->get();
        $productCategories = $product->categories->modelKeys();

        return view('products.edit', compact('product', 'categories', 'productCategories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $product = Product::findOrFail($id);

        if($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName   = time() . '.' . $photo->getClientOriginalExtension();
            $data['photo_url'] = $photo->storeAs('public/products_photos', $fileName, 'local');
            $data['photo_url'] = str_replace('public/', '', $data['photo_url']);
        }

        $data['can_discount'] = isset($data['can_discount']) ? true : false;
        $data['can_commission'] = isset($data['can_commission']) ? true : false;
        $data['is_replacement'] = isset($data['is_replacement']) ? true : false;
        $data['is_new'] = isset($data['is_new']) ? true : false;
        $data['is_active'] = isset($data['is_active']) ? true : false;
        $data['is_excl_serv'] = isset($data['is_excl_serv']) ? true : false;

        $data['discount_percentage'] =
        $data['can_discount'] ?
        $data['discount_percentage'] : null;

        $data['commission_percentage'] =
        $data['can_commission'] ?
        $data['commission_percentage'] : null;

        foreach($data['categories'] as $category) {
            $product->categories()->attach($category);
        }

        $product->update($data);

        return redirect()->route('products.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function delete($id)
    {

        $product = Product::findOrFail($id);

        return view('products.delete', compact('product'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);

        return redirect()->route('products.index');
    }
}
