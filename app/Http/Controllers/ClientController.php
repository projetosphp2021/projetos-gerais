<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Services\Client\DataTableClientService;
use App\Services\Client\IndexClientService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClientController extends Controller
{
    /**
     * @var IndexClientService
     */
    private $indexClientService;

    /**
     * ClientController constructor.
     * @param IndexClientService $indexClientService
     */
    public function __construct(IndexClientService $indexClientService)
    {
        $this->indexClientService = $indexClientService;
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request, DataTableClientService $dataTableClientService)
    {
        if($request->ajax()) {
            return $dataTableClientService->run();
        }

        $search = null;
        if ($request->search) {
            $search = $request->search;
        }
        $clients = $this->indexClientService->run($request);

        if ($request->expectsJson()) {
            return response($clients);
        }

        return view('clients.index')
            ->with('clients', $clients)
            ->with('search', $search);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $user = auth()->user();

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '.' . $photo->getClientOriginalExtension();
            $data['photo_url'] = $photo->storeAs('public/clients_photos', $fileName, 'local');
            $data['photo_url'] = str_replace('public/', '', $data['photo_url']);
        }

        $data['postcode'] = preg_replace('/[^0-9]/', '', $data['postcode']);
        $data['cpf'] = preg_replace('/[^0-9]/', '', $data['cpf']);
        $data['rg'] = preg_replace('/[^0-9]/', '', $data['rg']);
        $data['cellphone'] = preg_replace('/[^0-9]/', '', $data['cellphone']);
        $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
        $data['is_active'] = isset($data['is_active']) ? true : false;
        $newClient = new Client($data);

        $newClient->user_add_id = $user->id;
        $newClient->save();

        $newClient->address()->create($data);

        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return view('clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $client = Client::findOrFail($id);

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $fileName = time() . '.' . $photo->getClientOriginalExtension();
            $data['photo_url'] = $photo->storeAs('public/clients_photos', $fileName, 'local');
            $data['photo_url'] = str_replace('public/', '', $data['photo_url']);
        }

        $data['postcode'] = preg_replace('/[^0-9]/', '', $data['postcode']);
        $data['cpf'] = preg_replace('/[^0-9]/', '', $data['cpf']);
        $data['rg'] = preg_replace('/[^0-9]/', '', $data['rg']);
        $data['cellphone'] = preg_replace('/[^0-9]/', '', $data['cellphone']);
        $data['phone'] = preg_replace('/[^0-9]/', '', $data['phone']);
        $data['is_active'] = $data['is_active'] ? true : false;

        $client->update($data);

        $address = [
            'postcode' => $data['postcode'],
            'street' => $data['street'],
            'neighborhood' => $data['neighborhood'],
            'number' => $data['number'],
            'city' => $data['city'],
            'state' => $data['state'],
        ];

        $client->address()->update($address);

        return redirect()->route('clients.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     */
    public function delete($id)
    {

        $client = Client::findOrFail($id);

        return view('clients.delete', compact('client'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        Client::destroy($id);

        return redirect()->route('clients.index');
    }
}
