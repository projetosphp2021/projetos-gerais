<?php

namespace App\Http\Controllers;

use App\Http\Requests\Order\StoreOrderRequest;
use App\Http\Requests\Order\UpdateOrderRequest;
use App\Models\Order;
use App\Services\Order\DeleteOrderService;
use App\Services\Order\IndexOrderService;
use App\Services\Order\StoreOrderService;
use App\Services\Order\UpdateOrderService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * @var DeleteOrderService
     */
    private $deleteOrderService;

    /**
     * @var UpdateOrderService
     */
    private $updateOrderService;

    /**
     * @var StoreOrderService
     */
    private $storeOrderService;

    /**
     * @var IndexOrderService
     */
    private $indexOrderService;

    /**
     * OrderController constructor.
     * @param IndexOrderService $indexOrderService
     * @param StoreOrderService $storeOrderService
     * @param UpdateOrderService $updateOrderService
     * @param DeleteOrderService $deleteOrderService
     */
    public function __construct(
        IndexOrderService $indexOrderService,
        StoreOrderService $storeOrderService,
        UpdateOrderService $updateOrderService,
        DeleteOrderService $deleteOrderService)
    {
        $this->indexOrderService = $indexOrderService;
        $this->storeOrderService = $storeOrderService;
        $this->updateOrderService = $updateOrderService;
        $this->deleteOrderService = $deleteOrderService;
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $search = null;
        if ($request->search) {
            $search = $request->search;
        }
        $orders = $this->indexOrderService->run($request->all());
        return view('orders.index')
            ->with('orders', $orders)
            ->with('search', $search);
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('orders.create');
    }

    /**
     * @param StoreOrderRequest $storeOrderRequest
     * @return RedirectResponse
     */
    public function store(StoreOrderRequest $storeOrderRequest)
    {
        $data = $storeOrderRequest->validated();
        $this->storeOrderService->run($data);
        return redirect()->route('orders.index');
    }

    /**
     * @param Order $order
     * @return Application|Factory|View
     */
    public function edit(Order $order)
    {
        return view('orders.edit')
            ->with('order', $order);
    }

    /**
     * @param UpdateOrderRequest $updateOrderRequest
     * @param Order $order
     * @return RedirectResponse
     */
    public function update(UpdateOrderRequest $updateOrderRequest, Order $order)
    {
        $data = $updateOrderRequest->validated();
        $this->updateOrderService->run($data, $order);
        return redirect()->route('orders.index');
    }

    /**
     * @param Order $order
     * @return Application|ResponseFactory|Response
     */
    public function destroy(Order $order)
    {
        $this->deleteOrderService->run($order);
        return response(route('orders.index'));
    }
}
