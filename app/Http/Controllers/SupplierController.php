<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Supplier\{
    IndexSupplierService,
    StoreSupplierService,
    UpdateSupplierService,
    DeleteSupplierService,
};
use App\Models\Supplier;
use App\Http\Requests\SupplierRequest;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(IndexSupplierService $indexSupplierService)
    {
        $suppliers = $indexSupplierService->run();

        return view('suppliers.index', compact('suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SupplierRequest  $supplierRequest
     * @param  \App\Services\StoreSupplierService  $storeSupplierService
     * @return \Illuminate\Http\Response
     */
    public function store(SupplierRequest $supplierRequest, StoreSupplierService $storeSupplierService)
    {
        $data = $supplierRequest->validated();
        $storeSupplierService->run($data);

        return redirect()->route('suppliers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        return view('suppliers.edit', compact('supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SupplierRequest $supplierRequest, UpdateSupplierService $updateSupplierService, Supplier $supplier)
    {
        $data = $supplierRequest->validated();

        $updateSupplierService->run($supplier, $data);

        return redirect()->route('suppliers.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function delete(DeleteSupplierService $deleteSupplierSevice, Supplier $supplier)
    {
        return view('suppliers.delete', compact('supplier'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(DeleteSupplierService $deleteSupplierSevice, Supplier $supplier)
    {
        $deleteSupplierSevice->run($supplier);

        return redirect()->route('suppliers.index');
    }
}
