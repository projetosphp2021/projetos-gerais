<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required',
            'cnpj' => 'string|required|size:18',
            'photo' => 'mimes:jpg,jpeg,png',
            'state_registration' => 'string|nullable|size:15',
            'cellphone' => 'string|nullable',
            'phone' => 'string|nullable',
            'responsible_person' => 'string|nullable',
            'observation' => 'string|nullable',
            'postcode'  => 'string|nullable|size:9',
            'street' => 'string|nullable',
            'neighborhood' => 'string|nullable',
            'number' => 'numeric|nullable',
            'complement' => 'string|nullable',
            'city' => 'string|nullable',
            'state' => 'string|nullable',
        ];
    }
}
