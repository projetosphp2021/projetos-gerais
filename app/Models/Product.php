<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'code',
        'barcode',
        'name',
        'photo_url',
        'description',
        'price',
        'cost',
        'observation',
        'minimum_stock',
        'can_discount',
        'discount_percentage',
        'can_commission',
        'commission_percentage',
        'is_replacement',
        'is_new',
        'is_active',
        'type',
        'is_excl_serv'
    ];

    public function categories()
    {
       return $this->belongsToMany(Category::class, 'products_categories');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
}
