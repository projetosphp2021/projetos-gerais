<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;

    protected $table = 'sales';

    protected $fillable = [
        "sale_type",
        "status",
        "total",
        "salesman_id",
        "payment_method_id"
    ];

    //status da venda
    const STATUS_WAITING = 1;
    const STATUS_PAID = 2;
    const STATUS_CANCELLED = 3;

    //tipo da venda
    const SALE_TYPE_PRODUCT = 1;
    const SALE_TYPE_SERVICE = 2;

    public function items_sale(){
        return $this->hasMany(ItemsSale::class,'sale_id','id');
    }
}
