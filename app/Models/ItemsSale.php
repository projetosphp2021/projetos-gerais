<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemsSale extends Model
{
    use HasFactory;

    protected $table = 'items_sale';

    protected $fillable = [
        "sale_id",
        "product_id",
        "value",
        "discount",
        "salesman_comission",
        "user_approver_discount_id",
        "discount_justification",
    ];
}
