<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankCheck extends Model
{
    use HasFactory;

    protected $table = 'bank_check';

    protected $fillable = [
        "name",
        "check_number",
        "type_expense",
        "is_deposited",
        "value",
        "sale_id",
        "bank_account_id",
        "date_deposit"
    ];
}
