<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\MaskValueTrait;

class Client extends Model
{
    use HasFactory, SoftDeletes, MaskValueTrait;

    protected $fillable = [
        'full_name',
        'social_name',
        'gender',
        'photo_url',
        'cpf',
        'rg',
        'birthdate',
        'cellphone',
        'phone',
        'email',
        'observation',
        'profession',
        'is_active'
    ];

    public function address()
    {
        return $this->morphOne(Address::class, 'owner');
    }

    public function getCpfAttribute($value)
    {
        return $this->mask($value, '###.###.###-##');
    }

    public function getCellphoneAttribute($value)
    {
        return $this->mask($value, '(##) ####-####');
    }
}
