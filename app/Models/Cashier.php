<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cashier extends Model
{
    use HasFactory;

    protected $table = 'cashier';

    protected $fillable = [
        "start",
        "finish",
        "value",
        "change_value",
        "observation",
        "who_opened_id"
    ];
}
