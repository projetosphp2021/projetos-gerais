<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashierOut extends Model
{
    use HasFactory;

    protected $table = 'cashier_out';

    protected $fillable = [
        "cashier_id",
        "justification",
        "payment_method_id",
        "bank_accounts_id"
    ];
}
