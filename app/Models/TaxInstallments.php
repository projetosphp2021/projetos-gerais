<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxInstallments extends Model
{
    use HasFactory;

    protected $table = 'tax_installments';

    protected $fillable = [
        "date_start",
        "date_finish",
        "interest rate",
        "max_number_stallment"
    ];
}
