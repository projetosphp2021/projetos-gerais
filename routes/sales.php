<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('sales/get/all', 'App\Http\Controllers\SalesController@getDaySales');

});

