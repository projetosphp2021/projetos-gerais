<?php

use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\SystemConfigurationController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home'); 

    Route::get(
        'categories/{category}/delete',
        [App\Http\Controllers\CategoryController::class,
            'delete'
        ])->name('categories.delete');
    Route::resource('categories', App\Http\Controllers\CategoryController::class);

    Route::get(
        'clients/{client}/delete',
        [App\Http\Controllers\ClientController::class,
            'delete'
        ])->name('clients.delete');
    Route::resource('clients', App\Http\Controllers\ClientController::class);

    Route::get(
        'users/{user}/delete',
        [App\Http\Controllers\UserController::class,
            'delete'
        ])->name('users.delete');
    Route::resource('users', App\Http\Controllers\UserController::class);

    Route::get(
        'products/{product}/delete',
        [App\Http\Controllers\ProductController::class,
            'delete'
        ])->name('products.delete');
    Route::resource('products', ProductController::class);

    Route::resource('orders', OrderController::class);
    Route::get('pdv', [OrderController::class, 'create']);

    Route::get(
        'suppliers/{supplier}/delete',
        [App\Http\Controllers\SupplierController::class,
            'delete'
        ])->name('suppliers.delete');
    Route::resource('suppliers', App\Http\Controllers\SupplierController::class);

    Route::prefix('configurations')->name('configurations.')->group(function () {
        Route::get('system', [SystemConfigurationController::class, 'edit'])->name('system.edit');
        Route::put('system', [SystemConfigurationController::class, 'update'])->name('system.update');
    });
});

