/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from "vue";
import money from 'v-money'
Vue.use(money, {precision: 4})

window.Vue = require('vue').default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//ORDER
Vue.component('order-create', require('./components/Order/OrderCreate.vue').default);

//MAINTENANCE
Vue.component('maintenance-page', require('./components/Maintenance/Page.vue').default);

//CASHIER
Vue.component('cashier-payment', require('./components/Cashier/Payment.vue').default);

//RESOURCES
Vue.component('select-square-options', require('./components/Resources/SelectSquareOptions.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',

    methods:{
        collapseMenu(){
            $('.sidebar-mini').addClass('sidebar-collapse');
        }
    },

});
