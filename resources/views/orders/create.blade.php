@extends('adminlte::page')

@section('title', 'Pedidos')

@section('content_header')
    <h1>Pedidos</h1>
@endsection

@section('content')
    <order-create></order-create>
@endsection

@section('plugins.InputMask', true)
@section('js')
<script>
    $(":input").inputmask();
</script>
<script src="{{asset('js/app.js')}}"></script>
@endsection
