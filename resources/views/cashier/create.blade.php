
@extends('adminlte::page')

@section('title', 'Caixa')

@section('content')
    <cashier-payment logged-user="{{\Illuminate\Support\Facades\Auth::user()->name}}"></cashier-payment>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script src="{{asset('js/app.js')}}"></script>
@stop
