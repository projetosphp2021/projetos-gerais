@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
    <h1>Clientes</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Editar configurações</h3>
          </div>
          <form method="POST" enctype="multipart/form-data" action="{{route('configurations.system.update')}}">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group">
                <label for="company_name">Nome da Empresa</label>
                <input value="{{$configuration->company_name}}" type="text" class="form-control" name="company_name" id="company_name">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input value="{{$configuration->email}}" type="email" class="form-control" name="email" id="email">
              </div>
              <div class="form-group">
                <label for="photo">Logo</label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name="photo" id="photo">
                  <label class="custom-file-label" for="photo">Selecione um arquivo</label>
                </div>
              </div>
              <div class="form-group">
                  <label for="cnpj">CNPJ</label>
                  <input value="{{$configuration->cnpj}}" data-inputmask="'mask': '99.999.999/9999-99'" type="text" class="form-control" name="cnpj" id="cnpj">
              </div>
              <div class="form-group">
                  <label for="cellphone">Telefone</label>
                  <input value="{{$configuration->cellphone}}" data-inputmask="'mask': '(99) 9999-9999'" type="text" class="form-control" name="cellphone" id="cellphone">
              </div>
              <div class="form-group">
                  <label for="address">Endereço</label>
                  <input value="{{$configuration->address}}" type="text" class="form-control" name="address" id="address">
              </div>
              <div class="form-group">
                  <label for="bussiness_hours">Horário de Funcionamento</label>
                  <input value="{{$configuration->bussiness_hours}}" type="text" class="form-control" name="bussiness_hours" id="bussiness_hours">
              </div>
              <div class="form-group">
                  <label for="cupom_text">Texto de Cupom</label>
                  <input value="{{$configuration->cupom_text}}" type="text" class="form-control" name="cupom_text" id="cupom_text">
              </div>
              <div class="form-group">
                  <label for="warranty_text">Texto de Garantia</label>
                  <input value="{{$configuration->warranty_text}}" type="text" class="form-control" name="warranty_text" id="warranty_text">
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card-footer">
                @csrf
                @method('put')
                <button type="submit" class="btn btn-primary">Editar</button>
            </div>
        </form>
        <!-- /.card -->
      </div>
    </div>
</div>
@stop

@section('plugins.InputMask', true)

@section('js')
<script>
    $(":input").inputmask();
</script>
@stop
