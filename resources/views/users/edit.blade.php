@extends('adminlte::page')

@section('title', 'Usuários')

@section('content_header')
    <h1>Usuários</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Editar usuário - {{$user->name}}</h3>
          </div>
          <form method="POST" action="{{route('users.update', ['user' => $user->id])}}">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="form-group">
                  <label for="name">Nome</label>
                  <input value="{{$user->name}}" type="text" class="form-control" name="name" id="name">
              </div>
              <div class="form-group">
                  <label for="email">E-mail</label>
                  <input value="{{$user->email}}" type="email" class="form-control" name="email" id="email">
              </div>
              <div class="form-group">
                  <label for="password">Senha</label>
                  <input type="password" class="form-control" name="password" id="password">
              </div>
              <div class="form-group">
                <label for="group_id">Grupos</label>
                <select value="{{$user->group_id}}" name="group_id" id="group_id" class="form-control">
                  @foreach ($groups as $group)
                    <option value="{{$group->id}}">{{$group->name}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-check">
                <input checked="{{$user->is_active}}" type="checkbox" class="form-check-input" id="is_active" name="is_active">
                <label class="form-check-label" for="is_active">Ativo</label>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card-footer">
                @csrf
                @method('put')
                <a href="{{route('users.index')}}" class="btn btn-primary">Cancelar</a>
                <button type="submit" class="btn btn-primary">Editar</button>
            </div>
        </form>
        <!-- /.card -->
      </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)

@section('js')
@stop
