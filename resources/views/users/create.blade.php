
@extends('adminlte::page')

@section('title', 'Usuários')

@section('content_header')
    <h1>Usuários</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Cadastrar Novo Usuário</h3>
          </div>
          <form method="POST" action="/users">
            <!-- /.card-header -->
            <div class="card-body">
                <div class="form-group">
                  <label for="name">Nome</label>
                  <input type="text" class="form-control" name="name" id="name">
              </div>
              <div class="form-group">
                  <label for="email">E-mail</label>
                  <input type="email" class="form-control" name="email" id="email">
              </div>
              <div class="form-group">
                  <label for="password">Senha</label>
                  <input type="password" class="form-control" name="password" id="password">
              </div>
              <div class="form-group">
                <label for="role">Função</label>
                <select name="role" id="role" class="form-control">
                  @foreach ($roles as $role)
                    <option value="{{$role->name}}">{{ucfirst($role->name)}}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-check">
                <input type="checkbox" class="form-check-input" id="is_active" name="is_active">
                <label class="form-check-label" for="is_active">Ativo</label>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card-footer">
                @csrf
                <a href="{{route('users.index')}}" class="btn btn-primary">Cancelar</a>
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </form>
        <!-- /.card -->
      </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('plugins.Datatables', true)

@section('js')
@stop
