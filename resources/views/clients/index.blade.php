@extends('adminlte::page')

@section('title', 'Clientes')

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Lista de Clientes</h3>
            <div class="card-tools">
                <a href="{{route('clients.create')}}">Cadastrar Novo Cliente</a>
            </div>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table id="clients" class="table table-bordered table-hover mb-2"  style="width:100%">
              <thead>
              <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>E-mail</th>
                <th>CPF</th>
                <th>Data de Nascimento</th>
                <th>Celular</th>
                <th>Status</th>
                <th>Total Compras</th>
                <th>Ações</th>
              </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div>
</div>
@stop

@section('plugins.Datatables', true)

@section('js')
    <script>
        $( document ).ready(function() {
            console.log( "ready!" );
            $('#clients').DataTable({
            "paging": true,
            "lengthChange": false,
            "stripeClasses": [ 'bg-white', 'bg-light'],
            ajax: "{{ route('clients.index') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'client', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'cpf', name: 'CPF'},
                {data: 'birthdate', name: 'Data de Aniversário'},
                {data: 'cellphone', name: 'Celular'},
                {data: 'is_active', name: 'Status'},
                {data: 'total_purchases', name: 'Total Compras'},
                {data: 'action', name: 'Ações'},
            ],
            "searching": true,
            "language": {
                search: 'Pesquisar'
            },
            "ordering": true,
            "processing": true,
            "serverSide": true,
            "autoWidth": true,
            "responsive": true,
            "scrollX": true
            });
        });
    </script>
@stop
