
@extends('adminlte::page')

@section('title', 'Fornecedores')

@section('content_header')
    <h1>Fornecedores</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Cadastrar Novo Fornecedor</h3>
          </div>
          <form method="POST" enctype="multipart/form-data" action="/suppliers">
            <!-- /.card-header -->
            <div class="card-body">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
              <div class="form-group">
                <label for="name">Nome</label>
                <input type="text" class="form-control" name="name" id="name">
              </div>
              <div class="form-group">
                <label for="photo">Foto</label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name="photo" id="photo">
                  <label class="custom-file-label" for="photo">Selecione um arquivo</label>
                </div>
              </div>
              <div class="form-group">
                  <label for="cnpj">CNPJ</label>
                  <input data-inputmask="'mask': '99.999.999/9999-99'" type="text" class="form-control" name="cnpj" id="cpnj">
              </div>
              <div class="form-group">
                  <label for="state_registration">Inscrição Estadual</label>
                  <input  data-inputmask="'mask': '999.999.999.999'" type="text" class="form-control" name="state_registration" id="state_registration">
              </div>
              <div class="form-group">
                  <label for="cellphone">Celular</label>
                  <input data-inputmask="'mask': '(99) 99999-9999'" type="text" class="form-control" name="cellphone" id="cellphone">
              </div>
              <div class="form-group">
                  <label for="phone">Telefone</label>
                  <input data-inputmask="'mask': '(99) 9999-9999'" type="text" class="form-control" name="phone" id="phone">
              </div>
              <div class="form-group">
                  <label for="responsible_person">Responsável</label>
                  <input type="text" class="form-control" name="responsible_person" id="resonsible_person">
              </div>
              <div class="form-group">
                  <label for="observation">Observação</label>
                  <input type="text" class="form-control" name="observation" id="observation">
              </div>
              <h5 class="mt-2">Endereço</h5>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="postcode">CEP</label>
                  <input data-inputmask="'mask': '99999-999'" type="text" class="form-control" name="postcode" id="postcode">
                </div>
                <div class="form-group col-md-6">
                  <label for="street">Rua/Av.</label>
                  <input type="text" class="form-control" name="street" id="street">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="neighborhood">Bairro</label>
                  <input type="text" class="form-control" name="neighborhood" id="neighborhood">
                </div>
                <div class="form-group col-md-3">
                  <label for="number">Numero</label>
                  <input type="text" class="form-control" name="number" id="number">
                </div>
                <div class="form-group col-md-3">
                  <label for="complement">Complemento (Opcional)</label>
                  <input type="text" class="form-control" name="complement" id="complement">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="city">Cidade</label>
                  <input type="text" class="form-control" name="city" id="city">
                </div>
                <div class="form-group col-md-6">
                    <label for="state">Estado</label>
                    <select name="state" id="state" class="form-control">
                        @include('utils.states')
                    </select>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card-footer">
                @csrf
                <a href="{{route('clients.index')}}" class="btn btn-primary">Cancelar</a>
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </form>
        <!-- /.card -->
      </div>
    </div>
</div>
@stop

@section('plugins.bscustomfileinput', true)
@section('plugins.InputMask', true)

@section('js')
<script>
    $(document).ready(function () {
        bscustomfileinput.init();
    });

    $(":input").inputmask();
    $("#postcode").blur(function() {
         var postcode = $(this).val().replace(/\D/g, '');
         console.log(postcode)
        if (postcode.length === 8) {
            $("#street").val("...");
            $("#neighborhood").val("...");
            $("#city").val("...");
            $("#state").val("...");

            $.getJSON("https://viacep.com.br/ws/"+ postcode +"/json/?callback=?", function(data) {
                console.log(data)
                if (!("erro" in data)) {
                    $("#street").val(data.logradouro);
                    $("#neighborhood").val(data.bairro);
                    $("#city").val(data.localidade);
                    $("#state").val(data.uf);
                }
            });
        }
    });

</script>
@stop
