@extends('adminlte::page')

@section('title', 'Produtos')

@section('content_header')
    <h1>Produtos</h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Editar produto - {{$product->name}}</h3>
          </div>
          <form method="POST" action="{{route('products.update', ['product' => $product->id])}}">
            <!-- /.card-header -->
            <div class="card-body">
              <div class="form-group">
                <label for="name">Nome do Produto</label>
                <input value="{{$product->name}}" type="text" class="form-control" name="name" id="name">
              </div>
              <div class="form-group">
                <label for="photo">Foto</label>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name="photo" id="photo">
                  <label class="custom-file-label" for="photo">Selecione um arquivo</label>
                </div>
              </div>
              <div class="form-group">
                <label>Categorias</label>
                <select id="categories" name="categories[]" class="select2" multiple="multiple" data-placeholder="Selecione as Categorias" style="width: 100%;">
                  @foreach ($categories as $category)
                    <option selected="{{in_array($category->id, $productCategories)}}" value="{{$category->id}}">
                        {{$category->name}}
                    </option>
                    @foreach ($category->children as $subcategory)
                        <option selected="{{in_array($subcategory->id, $productCategories)}}" value="{{$subcategory->id}}">
                            -- {{$subcategory->name}}
                        </option>
                    @endforeach
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="code">Código do Produto</label>
                <input value="{{$product->code}}" type="text" class="form-control" name="code" id="code">
              </div>
              <div class="form-group">
                <label for="barcode">Código de Barra</label>
                <input value="{{$product->barcode}}" type="text" class="form-control" name="barcode" id="barcode">
              </div>
              <div class="form-group">
                <label for="description">Descrição</label>
                <input value="{{$product->description}}" type="text" class="form-control" name="description" id="description">
              </div>
              <div class="form-group">
                <label for="price">Valor</label>
                <input value="{{$product->price}}" type="text" class="form-control" name="price" id="price">
              </div>
              <div class="form-group">
                <label for="cost">Custo</label>
                <input value="{{$product->cost}}" type="text" class="form-control" name="cost" id="cost">
              </div>
              <div class="form-group">
                <label for="observation">Observação</label>
                <input value="{{$product->observation}}" type="text" class="form-control" name="observation" id="observation">
              </div>
              <div class="form-group">
                <label for="minimum_stock">Estoque Minimo</label>
                <input value="{{$product->minimum_stock}}" type="text" class="form-control" name="minimum_stock" id="minimum_stock">
              </div>
              <div class="form-group">
                <label for="type">Tipo de Medida</label>
                <select value="{{$product->type}}" name="type" id="type" class="form-control">
                    <option value="un">un</option>
                    <option value="kg">kg</option>
                    <option value="un">outro</option>
                </select>
              </div>
              <div class="form-check">
                <input checked="{{$product->can_discount}}" type="checkbox" class="form-check-input" id="can_discount" name="can_discount">
                <label class="form-check-label" for="can_discount">Aceita Desconto?</label>
              </div>
              <div class="form-group" id="discount_input">
                <label for="discount_percentage">Porcentagem Máxima de Desconto</label>
                <input value="{{$product->discount_percentage}}" type="text" class="form-control" name="discount_percentage" id="discount_percentage">
              </div>
              <div class="form-check">
                <input checked="{{$product->can_commission}}" type="checkbox" class="form-check-input" id="can_commission" name="can_commission">
                <label class="form-check-label" for="can_commission">Aceita Comissão?</label>
              </div>
              <div class="form-group" id="commission_input">
                <label for="commission_percentage">Porcentagem de Commisão</label>
                <input value="{{$product->commission_percentage}}" type="text" class="form-control" name="commission_percentage" id="commission_percentage">
              </div>
              <div class="form-check">
                <input checked="{{$product->is_replacement}}" type="checkbox" class="form-check-input" id="is_replacement" name="is_replacement">
                <label class="form-check-label" for="is_replacement">É Reposição?</label>
              </div>
              <div class="form-check">
                <input checked="{{$product->is_new}}" type="checkbox" class="form-check-input" id="is_new" name="is_new">
                <label class="form-check-label" for="is_new">É Novo?</label>
              </div>
              <div class="form-check">
                <input checked="{{$product->is_excl_serv}}" type="checkbox" class="form-check-input" id="is_excl_serv" name="is_excl_serv">
                <label class="form-check-label" for="is_excl_serv">É Serviço Exclusivo?</label>
              </div>
              <div class="form-check">
                <input checked="{{$product->is_active}}" type="checkbox" class="form-check-input" id="is_active" name="is_active">
                <label class="form-check-label" for="is_active">Ativo?</label>
              </div>
              <!-- /.card-body -->
            </div>
            <div class="card-footer">
                @method('put')
                @csrf
                <a href="{{route('products.index')}}" class="btn btn-primary">Cancelar</a>
                <button type="submit" class="btn btn-primary">Confirmar</button>
            </div>
        </form>
        <!-- /.card -->
      </div>
    </div>
</div>
@stop
@section('plugins.bscustomfileinput', true)
@section('plugins.Select2', true)

@section('js')
<script>
    $(document).ready(function () {
        $('.select2').select2();
        bscustomfileinput.init();
    });

    if($('#can_discount').is(':checked')) {
        $('#discount_input').show();
    } else {
        $('#discount_input').hide();
    }

    if($('#can_commission').is(':checked')) {
        $('#commission_input').show();
    } else {
        $('#commission_input').hide();
    }

    $('#can_discount').click(function() {
        if($(this).is(':checked')) {
            $('#discount_input').show();
        } else {
            $('#discount_input').hide();
        }
    });

    $('#can_commission').click(function() {
        if($(this).is(':checked')) {
            $('#commission_input').show();
        } else {
            $('#commission_input').hide();
        }
    });
</script>
@stop
